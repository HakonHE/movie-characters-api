package no.noroff.accelerate.moviecharactersapi.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.moviecharactersapi.models.Character;
import no.noroff.accelerate.moviecharactersapi.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/characters")
public class CharacterController {
    @Autowired
    private CharacterService characterService;

    @Operation(summary = "Get all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the characters",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) })
    })
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters() {
        return characterService.getAllCharacters();
    }

    @Operation(summary = "Get a character by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the character",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content) })
    @GetMapping("{id}")
    public ResponseEntity<Character> getCharacterById(@PathVariable int id) {
        return characterService.getCharacterById(id);
    }

    @Operation(summary = "Add a character")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created the character",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }) })
    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character) {
        return characterService.addCharacter(character);
    }

    @Operation(summary = "Update a character by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated the character",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request, character not found",
                    content = @Content) })
    @PutMapping("{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable int id, @RequestBody Character character) {
        return characterService.updateCharacter(id, character);
    }

    @Operation(summary = "Delete a character by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the character",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content) })
    @DeleteMapping("{id}")
    public ResponseEntity<Character> deleteCharacter(@PathVariable int id) {
        return characterService.deleteCharacter(id);
    }
}
