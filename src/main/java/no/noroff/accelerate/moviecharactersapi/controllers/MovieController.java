package no.noroff.accelerate.moviecharactersapi.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.moviecharactersapi.models.Character;
import no.noroff.accelerate.moviecharactersapi.models.Movie;
import no.noroff.accelerate.moviecharactersapi.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/movies")
public class MovieController {
    @Autowired
    private MovieService movieService;

    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the movies",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }) })
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        return movieService.getAllMovies();
    }

    @Operation(summary = "Get a movie by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "404", description = "Character not found",
                    content = @Content) })
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable int id) {
        return movieService.getMovieById(id);
    }

    @Operation(summary = "Get all characters in the movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the characters",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request, character not found",
                    content = @Content) })
    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Character>> getMovieCharacters(@PathVariable int id) {
        return movieService.getMoviesCharacters(id);
    }

    @Operation(summary = "Add a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created the movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }) })
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        return movieService.addMovie(movie);
    }

    @Operation(summary = "Update a movie by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated the movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request, movie not found",
                    content = @Content) })
    @PutMapping("{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable int id, @RequestBody Movie movie) {
        return movieService.updateMovie(id, movie);
    }

    @Operation(summary = "Update a movie's franchise by franchise id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated the movie franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @PatchMapping("{id}/franchise")
    public ResponseEntity<Movie> updateMovieFranchise(@PathVariable int id, @RequestBody int franchiseId) {
        return movieService.updateMovieFranchise(id, franchiseId);
    }

    @Operation(summary = "Updated a movie's character by a list of character id's")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated the movie's characters",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @PatchMapping("{id}/characters")
    public ResponseEntity<Movie> updateMovieCharacters(@PathVariable int id, @RequestBody List<Integer> characterIdList) {
        return movieService.updateMovieCharacters(id, characterIdList);
    }

    @Operation(summary = "Delete a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @DeleteMapping("{id}")
    public ResponseEntity<Movie> deleteMovie(@PathVariable int id) {
        return movieService.deleteMovie(id);
    }
}
