package no.noroff.accelerate.moviecharactersapi.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.moviecharactersapi.models.Character;
import no.noroff.accelerate.moviecharactersapi.models.Franchise;
import no.noroff.accelerate.moviecharactersapi.models.Movie;
import no.noroff.accelerate.moviecharactersapi.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/franchises")
public class FranchiseController {
    @Autowired
    private FranchiseService franchiseService;

    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the franchises",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) })})
    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        return franchiseService.getAllFranchises();
    }

    @Operation(summary = "Get a franchise by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @GetMapping("{id}")
    public ResponseEntity<Franchise> getFranchiseById(@Parameter(description = "id of franchise to be searched") @PathVariable int id) {
        return franchiseService.getFranchiseById(id);
    }

    @Operation(summary = "Get all the movies in the franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the movies",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request made, franchise not found",
                    content = @Content) })
    @GetMapping("/{id}/movies")
    public ResponseEntity<List<Movie>> getFranchiseMovies(@Parameter(description = "id of franchise to be searched") @PathVariable int id) {
        return franchiseService.getMoviesByFranchise(id);
    }

    @Operation(summary = "Get all the characters in the franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the characters",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request made, franchise not found",
                    content = @Content) })
    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Character>> getFranchiseMovieCharacters(@Parameter(description = "id of franchise to be searched")@PathVariable int id) {
        return franchiseService.getCharactersByFranchise(id);
    }

    @Operation(summary = "Add a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created the franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) })})
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@Parameter(description = "a franchise object")@RequestBody Franchise franchise) {
        return franchiseService.addFranchise(franchise);
    }

    @Operation(summary = "Update a franchise by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated the franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request, franchise not found",
                    content = @Content) })
    @PutMapping("{id}")
    public ResponseEntity<Franchise> updateFranchise(@Parameter(description = "id of franchise to be searched") @PathVariable int id, @Parameter(description = "a franchise object") @RequestBody Franchise franchise) {
        return franchiseService.updateFranchise(id, franchise);
    }

    @Operation(summary = "Delete a franchise by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @DeleteMapping("{id}")
    public ResponseEntity<Franchise> deleteFranchise(@Parameter(description = "id of franchise to be searched") @PathVariable int id) {
        return franchiseService.deleteFranchise(id);
    }
}
