package no.noroff.accelerate.moviecharactersapi.seeders;

import no.noroff.accelerate.moviecharactersapi.models.Movie;
import no.noroff.accelerate.moviecharactersapi.repositories.MovieRepository;
import no.noroff.accelerate.moviecharactersapi.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class MovieSeeder implements ApplicationRunner {
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    MovieService movieService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        movieSeeder();
    }

    private void movieSeeder() {
        if (movieRepository.count() == 0) {
            Movie movie1 = new Movie("The Lord of the Rings: The Fellowship of the Ring", "Adventure, Fantasy, Action",
                    2001, "Peter Jackson", "https://images-na.ssl-images-amazon.com/images/S/pv-target-images/63006f8c3192e6f732b9f699f5ed368fb557e6742ca8c01209fef0c046d91113._RI_V_TTW_.jpg",
                    "https://youtu.be/cKEGZ-CvWHk");
            movie1.addFranchise(movieService.updateMovieFranchise(1));
            movie1.addCharacter(movieService.updateMovieCharacter(1));
            Movie movie2 = new Movie("The Lord of the Rings: The Two Towers", "Adventure, Drama, Action",
                    2002, "Peter Jackson", "https://i.pinimg.com/originals/58/6f/19/586f19b2cabf18ba2d9fbdbf14cf1b27.jpg",
                    "https://youtu.be/hYcw5ksV8YQ");
            movie2.addFranchise(movieService.updateMovieFranchise(1));
            movie2.addCharacter(movieService.updateMovieCharacter(1));
            Movie movie3 = new Movie("The Lord of the Rings: The Return of the King", "Adventure, Drama, Action",
                    2003, "Peter Jackson", "https://i5.walmartimages.com/asr/e831ba45-87de-471f-98be-2959d66cc433_1.20b2d4a6b510c4175b23ea8c147eb72f.jpeg",
                    "https://youtu.be/KOQSQaGgJxs");
            movie3.addFranchise(movieService.updateMovieFranchise(1));
            movie3.addCharacter(movieService.updateMovieCharacter(1));
            movieRepository.save(movie1);
            movieRepository.save(movie2);
            movieRepository.save(movie3);
        }
    }
}
