package no.noroff.accelerate.moviecharactersapi.seeders;

import no.noroff.accelerate.moviecharactersapi.models.Franchise;
import no.noroff.accelerate.moviecharactersapi.repositories.FranchiseRepository;
import no.noroff.accelerate.moviecharactersapi.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class FranchiseSeeder implements ApplicationRunner {
    @Autowired
    FranchiseRepository franchiseRepository;

    @Autowired
    FranchiseService franchiseService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        franchiseSeeder();
    }

    private void franchiseSeeder() {
        if (franchiseRepository.count() == 0) {
            Franchise franchise1 = new Franchise("Lord of The Rings", "The trilogy");
            Franchise franchise2 = new Franchise("Harry Potter", "A magical 8 movie series");
            Franchise franchise3 = new Franchise("Star Wars", "A space fantasy in a lot of parts");
            franchiseRepository.save(franchise1);
            franchiseRepository.save(franchise2);
            franchiseRepository.save(franchise3);
        }
    }
}
