package no.noroff.accelerate.moviecharactersapi.seeders;

import no.noroff.accelerate.moviecharactersapi.models.Character;
import no.noroff.accelerate.moviecharactersapi.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class CharacterSeeder implements ApplicationRunner {
    @Autowired
    CharacterRepository characterRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        characterSeeder();
    }

    private void characterSeeder() {
        if (characterRepository.count() == 0) {
            Character character1 = new Character("Frodo Baggins", null,
                    "male", "https://m.media-amazon.com/images/M/MV5BODM1MGNhY2UtZDYzMS00Y2ZmLWEwNTQtNjUxYWU4MzdjY2UyXkEyXkFqcGdeQXVyMzQ3Nzk5MTU@._V1_SY200_CR23,0,200,200_AL_.jpg");
            Character character2 = new Character("Samwise Gamgee", "Sam",
                    "male", "https://m.media-amazon.com/images/M/MV5BMTQzMDMxODgzM15BMl5BanBnXkFtZTcwMzU2MTk2Mw@@._V1_SY200_CR50,0,200,200_AL_.jpg");
            Character character3 = new Character("Smeagol", "Gollum",
                    "male", "https://th.bing.com/th/id/OIP.GVtE3B_eoY7yW1hhGSFU_gHaEK?pid=ImgDet&rs=1");
            Character character4 = new Character("Gandalf the Grey", "Gandalf the White",
                    "male", "https://fromthestage.net/wp-content/uploads/2021/03/ian-mckellen-the-lord-of-the-rings-2021.jpg");
            Character character5 = new Character("Legolas", null,
                    "male", "https://tse1.mm.bing.net/th/id/OIP.4TSMVGLl6jX-eS1GhFxCZwHaFp?pid=ImgDet&rs=1");
            Character character6 = new Character("Aragorn", "Strider",
                    "male", "https://www.theonering.com/wp-content/uploads/2020/11/aragorn-anduril-1280x940.jpg");
            characterRepository.save(character1);
            characterRepository.save(character2);
            characterRepository.save(character3);
            characterRepository.save(character4);
            characterRepository.save(character5);
            characterRepository.save(character6);
        }
    }
}
