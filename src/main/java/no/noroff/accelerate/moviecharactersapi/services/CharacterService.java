package no.noroff.accelerate.moviecharactersapi.services;

import no.noroff.accelerate.moviecharactersapi.models.Character;
import no.noroff.accelerate.moviecharactersapi.models.Movie;
import no.noroff.accelerate.moviecharactersapi.repositories.CharacterRepository;
import no.noroff.accelerate.moviecharactersapi.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterService {
    @Autowired
    private CharacterRepository characterRepository;
    @Autowired
    private MovieRepository movieRepository;

    /**
     * Gets all the characters.
     * @return ResponseEntity with a list of characters and a status code.
     */
    public ResponseEntity<List<Character>> getAllCharacters() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characterRepository.findAll(), status);
    }

    /**
     * Gets a character by the id.
     * @param id
     * @return ResponseEntity with a character object and a status code.
     */
    public ResponseEntity<Character> getCharacterById(int id) {
        Character returnCharacter = new Character();
        HttpStatus status;

        if(characterRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnCharacter = characterRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(returnCharacter, status);
    }

    /**
     * Adds a character with the provided character object.
     * @param character
     * @return ResponseEntity with a character object and a status code.
     */
    public ResponseEntity<Character> addCharacter(Character character) {
        Character returnCharacter = characterRepository.save(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnCharacter, status);
    }

    /**
     * Updates a character by the id and a character object.
     * @param id
     * @param character
     * @return ResponseEntity with a character object and a status code.
     */
    public ResponseEntity<Character> updateCharacter(int id, Character character) {
        Character returnCharacter = new Character();
        HttpStatus status;

        if(!characterRepository.existsById(id)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter, status);
        }
        returnCharacter = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnCharacter, status);
    }

    /**
     * Deletes a character by the id.
     * @param id
     * @return ResponseEntity with a status code.
     */
    public ResponseEntity<Character> deleteCharacter(int id) {
        Character character = new Character();
        HttpStatus status;
        if(characterRepository.existsById(id)) {
            character = characterRepository.findById(id).get();
            List<Integer> movieList = character.getMovies();
            for (Integer integer : movieList) {
                if(!movieRepository.existsById(integer)) {
                    status = HttpStatus.NOT_FOUND;
                    return new ResponseEntity<>(status);
                }
                Movie movie = movieRepository.findById(integer).get();
                movie.removeCharacter(character);
                movieRepository.save(movie);
            }

            characterRepository.deleteById(id);
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }
}
