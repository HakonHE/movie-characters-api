package no.noroff.accelerate.moviecharactersapi.services;

import no.noroff.accelerate.moviecharactersapi.models.Character;
import no.noroff.accelerate.moviecharactersapi.models.Franchise;
import no.noroff.accelerate.moviecharactersapi.models.Movie;
import no.noroff.accelerate.moviecharactersapi.repositories.CharacterRepository;
import no.noroff.accelerate.moviecharactersapi.repositories.FranchiseRepository;
import no.noroff.accelerate.moviecharactersapi.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class FranchiseService {
    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharacterRepository characterRepository;

    /**
     * Gets all the franchises.
     * @return ResponseEntity with a list of franchises and a status code.
     */
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchiseRepository.findAll(), status);
    }

    /**
     * Gets a franchise by the id.
     * @param id
     * @return ResponseEntity with a franchise object and a status code.
     */
    public ResponseEntity<Franchise> getFranchiseById(int id) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if (franchiseRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(returnFranchise, status);
    }

    /**
     * Gets the movies in the franchise
     * @param franchiseId
     * @return ResponseEntity with a list of movies and a status code.
     */
    public ResponseEntity<List<Movie>> getMoviesByFranchise(int franchiseId) {
        Franchise franchise = new Franchise();
        HttpStatus status;

        if (franchiseRepository.existsById(franchiseId)) {
            franchise = franchiseRepository.findById(franchiseId).get();
            status = HttpStatus.OK;
            return new ResponseEntity<>(movieRepository.findAllById(franchise.getMovies()), status);
        }
        status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(null, status);
    }

    /**
     * Gets all the characters in the franchise.
     * @param franchiseId
     * @return ResponseEntity with a list of characters and a status code.
     */
    public ResponseEntity<List<Character>> getCharactersByFranchise(int franchiseId) {
        Franchise franchise = new Franchise();
        HttpStatus status;

        if (franchiseRepository.existsById(franchiseId)) {
            franchise = franchiseRepository.findById(franchiseId).get();
            status = HttpStatus.OK;
            List<Movie> movieList = movieRepository.findAllById(franchise.getMovies());
            List<Character> characterList = new ArrayList<>();
            for (Movie movie : movieList) {
                if (movieRepository.existsById(movie.getId())) {
                    if(!characterList.containsAll(characterRepository.findAllById(movie.getCharacters()))){
                        characterList.addAll(characterRepository.findAllById(movie.getCharacters()));
                    }
                }
            }
            return new ResponseEntity<>(characterList, status);
        }
        status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(null, status);
    }

    /**
     * Adds a franchise by getting a franchise object.
     * @param franchise
     * @return ResponseEntity with a franchise object and a status code.
     */
    public ResponseEntity<Franchise> addFranchise(Franchise franchise) {
        Franchise returnFranchise = franchiseRepository.save(franchise);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnFranchise, status);
    }

    /**
     * Updates a franchise by the id.
     * @param id
     * @param franchise
     * @return ResponseEntity with a franchise object and a status code.
     */
    public ResponseEntity<Franchise> updateFranchise(int id, Franchise franchise) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if(!franchiseRepository.existsById(id)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise, status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise, status);
    }

    /**
     * Deletes a franchise by the id.
     * @param id
     * @return ResponseEntity with a status code.
     */
    public ResponseEntity<Franchise> deleteFranchise(int id) {
        Franchise franchise = new Franchise();
        HttpStatus status;
        if(franchiseRepository.existsById(id)) {
            franchise = franchiseRepository.getById(id);

            List<Integer> movieList = franchise.getMovies();
            for (Integer integer : movieList) {
                Movie movie = movieRepository.findById(integer).get();
                movie.setterFranchise(null);
                movieRepository.save(movie);
            }

            franchiseRepository.deleteById(id);
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }

}

