package no.noroff.accelerate.moviecharactersapi.services;

import no.noroff.accelerate.moviecharactersapi.models.Character;
import no.noroff.accelerate.moviecharactersapi.models.Franchise;
import no.noroff.accelerate.moviecharactersapi.models.Movie;
import no.noroff.accelerate.moviecharactersapi.repositories.CharacterRepository;
import no.noroff.accelerate.moviecharactersapi.repositories.FranchiseRepository;
import no.noroff.accelerate.moviecharactersapi.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieService {
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private CharacterRepository characterRepository;

    /**
     * Get all the movies.
     * @return ResponseEntity with a list of the movies and a status code.
     */
    public ResponseEntity<List<Movie>> getAllMovies() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movieRepository.findAll(), status);
    }

    /**
     * Gets a movie by an id.
     * @param id
     * @return ResponseEntity with a movie and a status.
     */
    public ResponseEntity<Movie> getMovieById(int id) {
        Movie returnMovie = new Movie();
        HttpStatus status;

        if(movieRepository.existsById(id)) {
            status = HttpStatus.OK;
            returnMovie = movieRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Gets the movies characters by the movies id.
     * @param movieId
     * @return A list of characters and a status code.
     */
    public ResponseEntity<List<Character>> getMoviesCharacters(int movieId) {
        Movie movie = new Movie();
        HttpStatus status;

        if (movieRepository.existsById(movieId)) {
            movie = movieRepository.findById(movieId).get();
            status = HttpStatus.OK;
            return new ResponseEntity<>(characterRepository.findAllById(movie.getCharacters()), status);
        }
        status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(null, status);
    }

    /**
     * Adds a new movie based on a movie object.
     * @param movie
     * @return ResponseEntity with a newly created movie object and a status code.
     */
    public ResponseEntity<Movie> addMovie(Movie movie) {
        Movie returnMovie = movieRepository.save(movie);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Updates an already existing movie by the movie id and a movie object.
     * @param id
     * @param movie
     * @return ResponseEntity with a movie object and a status code.
     */
    public ResponseEntity<Movie> updateMovie(int id, Movie movie) {
        Movie returnMovie = new Movie();
        HttpStatus status;

        if(!movieRepository.existsById(id)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }

        if(!franchiseRepository.existsById(movie.getFranchise().getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        movie.setFranchise(franchiseRepository.findById(movie.getFranchise().getId()).get());
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * Updates the movies franchise.
     * @param id
     * @param franchiseId
     * @return ResponseEntity with a movie object and a status code.
     */
    public ResponseEntity<Movie> updateMovieFranchise(int id, int franchiseId) {
        HttpStatus status;
        Movie movie = new Movie();
        if(movieRepository.existsById(id)) {
            movie = movieRepository.findById(id).get();
            movie.addFranchise(updateMovieFranchise(franchiseId));

            movie = movieRepository.save(movie);
            status = HttpStatus.NO_CONTENT;
        }
        else{
            status = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(movie, status);
    }

    /**
     * Finds the franchise if it exists and gets it.
     * @param franchiseId
     * @return franchise if found or else null.
     */
    public Franchise updateMovieFranchise(Integer franchiseId) {
        if(franchiseRepository.existsById(franchiseId)) {
            return franchiseRepository.findById(franchiseId).get();
        }
        return null;
    }

    /**
     * Updates the movies characters.
     * @param id
     * @param characterIdList
     * @return ResponseEntity with a movie object and a status code.
     */
    public ResponseEntity<Movie> updateMovieCharacters(int id, List<Integer> characterIdList) {
        HttpStatus status;
        Movie movie = new Movie();
        if(movieRepository.existsById(id)) {
            movie = movieRepository.findById(id).get();

            if(!characterIdList.isEmpty()) {
                for (Integer integer : characterIdList) {
                    movie.addCharacter(updateMovieCharacter(integer));
                }
            }
            movie = movieRepository.save(movie);
            status = HttpStatus.NO_CONTENT;
        }
        else{
            status = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(movie, status);
    }

    /**
     * Finds the character by the id and checks if it exists then gets it.
     * @param characterId
     * @return A character object if found or else null.
     */
    public Character updateMovieCharacter(Integer characterId) {
        if(characterRepository.existsById(characterId)) {
            return characterRepository.findById(characterId).get();
        }
        return null;
    }

    /**
     * Deletes the movie by the id.
     * @param id
     * @return ResponseEntity with a status code.
     */
    public ResponseEntity<Movie> deleteMovie(int id) {
        HttpStatus status;
        if(movieRepository.existsById(id)) {
            movieRepository.deleteById(id);
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }
}
