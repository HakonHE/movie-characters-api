package no.noroff.accelerate.moviecharactersapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String movieTitle;
    private String Genre;
    private int releaseYear;
    private String director;
    private String picture;
    private String trailer;
    // Relationships
    @ManyToMany
    @JoinTable(
            name = "Movie_Character",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    public Set<Character> characters = new HashSet<>();
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise = new Franchise();

    /**
     * A getter for the characters in the movie.
     * @return a list of characters by their id
     */
    @JsonGetter("characters")
    public List<Integer> getCharacters() {
        if(characters == null)
            return null;
        return characters.stream().map(c -> c.getId()).collect(Collectors.toList());
    }

    /**
     * Adds a single character to the character set.
     * @param character
     */
    public void addCharacter(Character character) {
        this.characters.add(character);
    }

    /**
     * Removes a single character from the character set.
     * @param character
     */
    public void removeCharacter(Character character) {
        this.characters.remove(character);
    }

    /**
     * Replaces the current set with a new one.
     * @param characters
     */
    @JsonSetter("characters")
    public void setCharacters(Set<Character> characters) {
        this.characters = characters;
    }

    @JsonGetter("franchise")
    public Integer getFranchiseGetter() {
        if(franchise == null)
            return null;
        return franchise.getId();
    }

    public void addFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    @JsonSetter("franchise")
    public void setterFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public Movie() {
    }

    public Movie(String movieTitle, String genre, int releaseYear, String director, String picture, String trailer) {
        this.movieTitle = movieTitle;
        Genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.picture = picture;
        this.trailer = trailer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String genre) {
        Genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTrailer() {
        return trailer;
    }


    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

}
