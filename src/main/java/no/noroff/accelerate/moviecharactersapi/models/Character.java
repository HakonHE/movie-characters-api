package no.noroff.accelerate.moviecharactersapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String fullName;
    private String alias;
    private String gender;
    private String picture;
    // Relationships
    @ManyToMany(mappedBy = "characters")
    public Set<Movie> movies = new HashSet<>();

    @JsonGetter("movies")
    public List<Integer> getMovies() {
        if(movies == null)
            return null;
        return movies.stream().map(m -> m.getId()).collect(Collectors.toList());
    }

    public Character() {
    }

    public Character(String fullName, String alias, String gender, String picture) {
        this.fullName = fullName;
        this.alias = alias;
        this.gender = gender;
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
