# Movie Characters API

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

An api for getting characters that played in movies, and the franchises the movies are a part of.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)

## Background

This project is the third assignment of the Java Full Stack course. 

#### The task was to create a web API. With the specifications:

* Create models and Repositories to cater for the specifications in Appendix A.
* Create representations of domain object as to not overexpose your database data, this is detailed in Appendix B.
* Create controllers according to specifications in Appendix B.
* Swagger/Open API documentation.


#### The API features are:

* Get either characters, movies or franchises.
* CRUD functionality.
* Get characters by franchise, movie by franchise or characters by movie.

## Install

* Install JDK 17
* Install Intellij
* Clone repository
* Install PostgreSQL
* Install Postman

## Usage

* Run the application
* Use endpoints in Postman or in browser
* Heroku app link (e.g characters: https://movie-characters-api-hhe.herokuapp.com/api/v1/characters)

## Maintainers

[@HåkonHE](https://gitlab.com/HåkonHE)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.
